"""
This sub-module contains methods to simulate second-generation shotgun sequencing, and tools to
visualize and evaluate fragment assembly.
"""
