
from __future__ import annotations
from abc import ABC


class Record ( ABC ) :
    """
    Record is a piece of information that can be indexed in a Registry.
    """
    pass

    # @abstractmethod
    # def clone ( self ) -> Record :
    #     pass
