=======
Credits
=======

Development Lead
----------------

* Ulf Liebal <ulf.liebal@rwth-aachen.de>

Contributors
------------

* Rafael Schimassek <rafael.schimassek@rwth-aachen.de>
* Lars Blank


Helpful Tools
-------------

This package was initialized with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
