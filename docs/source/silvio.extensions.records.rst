silvio.extensions.records package
=================================

.. automodule:: silvio.extensions.records
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   silvio.extensions.records.gene

Submodules
----------

silvio.extensions.records.growth\_record module
-----------------------------------------------

.. automodule:: silvio.extensions.records.growth_record
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
