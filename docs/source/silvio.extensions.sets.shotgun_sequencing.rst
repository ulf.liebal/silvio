silvio.extensions.sets.shotgun\_sequencing package
==================================================

.. automodule:: silvio.extensions.sets.shotgun_sequencing
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

silvio.extensions.sets.shotgun\_sequencing.assembly module
----------------------------------------------------------

.. automodule:: silvio.extensions.sets.shotgun_sequencing.assembly
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

silvio.extensions.sets.shotgun\_sequencing.datatype module
----------------------------------------------------------

.. automodule:: silvio.extensions.sets.shotgun_sequencing.datatype
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

silvio.extensions.sets.shotgun\_sequencing.evaluation module
------------------------------------------------------------

.. automodule:: silvio.extensions.sets.shotgun_sequencing.evaluation
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

silvio.extensions.sets.shotgun\_sequencing.sequencing module
------------------------------------------------------------

.. automodule:: silvio.extensions.sets.shotgun_sequencing.sequencing
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

silvio.extensions.sets.shotgun\_sequencing.storage module
---------------------------------------------------------

.. automodule:: silvio.extensions.sets.shotgun_sequencing.storage
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

silvio.extensions.sets.shotgun\_sequencing.visualization module
---------------------------------------------------------------

.. automodule:: silvio.extensions.sets.shotgun_sequencing.visualization
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
