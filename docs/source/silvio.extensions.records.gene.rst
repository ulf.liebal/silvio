silvio.extensions.records.gene package
======================================

.. automodule:: silvio.extensions.records.gene
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

silvio.extensions.records.gene.crafted\_gene module
---------------------------------------------------

.. automodule:: silvio.extensions.records.gene.crafted_gene
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

silvio.extensions.records.gene.gene module
------------------------------------------

.. automodule:: silvio.extensions.records.gene.gene
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

silvio.extensions.records.gene.localized\_gene module
-----------------------------------------------------

.. automodule:: silvio.extensions.records.gene.localized_gene
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

silvio.extensions.records.gene.stub\_gene module
------------------------------------------------

.. automodule:: silvio.extensions.records.gene.stub_gene
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
